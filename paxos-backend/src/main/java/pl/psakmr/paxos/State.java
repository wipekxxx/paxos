package pl.psakmr.paxos;

import lombok.*;

import java.io.Serializable;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
@Builder
public class State implements Serializable {
    private String num;
    private String value;
    private boolean isLeader;
    private boolean isRunning;
}

