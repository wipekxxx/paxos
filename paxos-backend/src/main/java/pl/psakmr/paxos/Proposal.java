package pl.psakmr.paxos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.io.Serializable;

@AllArgsConstructor
@Getter
@ToString
public class Proposal implements Serializable {
    private int csn;
    private int psn;
    private String value;
}
