package pl.psakmr.paxos;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@RestController
@CrossOrigin(origins = {"http://localhost:4200", "http://localhost:8080"})
public class PaxosController {

    public static final boolean isDebugging = true;

    private static Set<Node> nodes = new HashSet<Node>();;
    private static Set<NodeLocationData> nodeLocations = new HashSet<>();
    private static ArrayList<String> proposeBuffer = new ArrayList<>();
    private static boolean isRunning;

    // Test case variables
    private static final int testTime = 20;
    public static boolean slotSkippingFlag = false;

    @PostMapping("/init")
    public ResponseEntity<String> init() {
        stopAll();
        clearStableStorage();

        for (int i = 0; i < 3; i++) {
            Node node = new Node(i);
            if (i == 0)
                node.becomeLeader();
            nodes.add(node);
            nodeLocations.add(node.getLocationData());
        }

        for (Node node : nodes)
            node.setNodeList(nodeLocations);

        return ResponseEntity.ok().build();
    }

    @PostMapping("/start")
    public ResponseEntity<String> start() {
        startAll();
        return ResponseEntity.ok().build();
    }

    @PostMapping("/stop")
    public ResponseEntity<String> stop() {
        stopAll();
        clearStableStorage();
        return ResponseEntity.ok().build();
    }

    @PostMapping("/propose/{value}")
    public ResponseEntity<String> proposeValue(@PathVariable String value) {
        if (isRunning)
            propose(value);
        else
            proposeBuffer.add(value);
        return ResponseEntity.ok().build();
    }

    @GetMapping
    public List<State> getState() {
        return nodes.stream().map(node -> State.builder()
                .num(String.valueOf(node.getLocationData().getNum()))
                .isLeader(node.isLeader())
                .value(value(node.getValues()))
                .isRunning(node.isRunning())
                .build()).collect(Collectors.toList());
    }

    private String value(Map<Integer, String> values) {
        Integer max = values.keySet().isEmpty() ? 0 : Collections.max(values.keySet());
        return values.getOrDefault(max,"");
    }

    @PostMapping("/test/leader-fail")
    public ResponseEntity<String> leaderFail() {
        testLeaderFail();
        return ResponseEntity.ok().build();
    }

    @PostMapping("/test/simultaneous-fail")
    public ResponseEntity<String> simultaneousFail() {
        testSimultaneousFail();
        return ResponseEntity.ok().build();
    }

    @PostMapping("/test/slot-skip")
    public ResponseEntity<String> slotSkip() {
        testSlotSkipping();
        return ResponseEntity.ok().build();
    }

    private static void testLeaderFail() {
        new Thread(() -> {
            // don't start timer until it's running
            while (!isRunning)
                Thread.yield(); // so the while loop doesn't spin too much
            log.info("Leader Fail Test Started");

            // fail timer
            long expireTime = System.currentTimeMillis() + testTime;
            boolean isRunning = true;
            while (isRunning) {
                if (expireTime < System.currentTimeMillis()) {
                    stop(0);
                    isRunning = false;
                }
                Thread.yield(); // so the while loop doesn't spin too much
            }
        }).start();
    }

    private static void testSimultaneousFail() {
        new Thread(() -> {
            // don't start timer until it's running
            while (!isRunning)
                Thread.yield(); // so the while loop doesn't spin too much
            log.info("Simultaneous Fail Test Started");

            // fail timer
            long expireTime = System.currentTimeMillis() + testTime;
            boolean isRunning = true;
            while (isRunning) {
                if (expireTime < System.currentTimeMillis()) {
                    stop(0);
                    stop(1);
                    isRunning = false;
                }
                Thread.yield(); // so the while loop doesn't spin too much
            }
        }).start();
    }

    private static void testSlotSkipping() {
        // implementation is in Node.propose()
        slotSkippingFlag = true;
    }

    private static void stop(int n) {
        log.info("Stopping node " + n);
        for (Node node : nodes)
            if (node.getLocationData().getNum() == n) {
                node.stop();
                break;
            }
    }

    private static void startAll() {
        log.info("Starting all nodes...");

        nodes.forEach(Node::start);

        while (proposeBuffer.size() > 0)
            propose(proposeBuffer.remove(0));

        isRunning = true;

        log.info("All nodes started");
    }

    private static void propose(String s) {
        log.info("Proposing: " + s);
        for (Node node : nodes)
            if (node.isLeader()) {
                node.propose(s);
                break;
            }
    }

    private static void stopAll() {
        log.info("Stopping all nodes...");

        nodes.forEach(Node::stop);
        nodes.clear();
        nodeLocations.clear();
        isRunning = false;

        log.info("All nodes stopped");
    }

  private static void clearStableStorage() {
      nodes.forEach(Node::clearStableStorage);
  }
}
