package pl.psakmr.paxos;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public abstract class Message implements Serializable {
    protected NodeLocationData sender;
    protected NodeLocationData reciever;
}
