package pl.psakmr.paxos;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class PrepareRequestMessage extends Message {
    private int csn;
    private int psn;
}
