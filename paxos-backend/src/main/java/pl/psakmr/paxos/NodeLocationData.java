package pl.psakmr.paxos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@AllArgsConstructor
public class NodeLocationData implements Serializable {
    private String host;
    private int port;
    private int num;
    private boolean isLeader;

    public void becomeLeader() {
        isLeader = true;
    }

    public void becomeNonLeader() {
        isLeader = false;
    }

    public String toString()
    {
        return ((Integer)num).toString();
    }
}
