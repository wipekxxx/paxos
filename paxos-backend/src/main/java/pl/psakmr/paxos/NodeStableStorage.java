package pl.psakmr.paxos;

import java.io.Serializable;
import java.util.Map;

public class NodeStableStorage implements Serializable {
    public Map<Integer, Integer> minPsns;
    public Map<Integer, Proposal> maxAcceptedProposals;
}
