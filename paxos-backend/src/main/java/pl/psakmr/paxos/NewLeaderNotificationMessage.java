package pl.psakmr.paxos;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class NewLeaderNotificationMessage extends Message {
    private int num;
}
