package pl.psakmr.paxos;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class PrepareResponseMessage extends Message {
    private int csn;
    private int minPsn;
    private Proposal proposal;
}
