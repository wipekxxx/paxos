package pl.psakmr.paxos;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class AcceptNotificationMessage extends Message {
    private Proposal proposal;
}
