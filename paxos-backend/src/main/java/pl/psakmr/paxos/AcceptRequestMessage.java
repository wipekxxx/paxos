package pl.psakmr.paxos;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class AcceptRequestMessage extends Message {
    private Proposal proposal;
}
