import {Component} from '@angular/core';
import {Node} from './paxos/node';
import {Client} from './paxos/client';
import {AppserviceService} from './appservice.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'paxos';

  nodes: Node<number>[];
  client: Client;
  state: State[] = [];

  constructor(private service: AppserviceService) {
    service.init().subscribe();
    service.start().subscribe();
    this.start(0);
  }

  start(value: number) {
    setTimeout(() => {
      this.service.propose('' + value).subscribe();
      this.service.state().subscribe(data => {
        console.log(data);
        this.state = data;
      });
      this.start(value + 1);
    }, 1000);
  }

  leaderFail() {
    this.service.leaderFail().subscribe();
  }

  simulateFail() {
    this.service.simultaneousFail().subscribe();
  }
}

export class State {
  num: string;
  value: string;
  leader: boolean;
  running: boolean;
}
