import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {State} from './app.component';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AppserviceService {

  path = 'http://localhost:8080/';

  constructor(private http: HttpClient) { }

  init(): Observable<any> {
    const headers = new HttpHeaders()
      .append('Content-Type', 'application/json')
      .append('Accept', 'application/json');
    return this.http.post(`${this.path}init`, null, { headers });
  }

  start(): Observable<any> {
    const headers = new HttpHeaders()
      .append('Content-Type', 'application/json')
      .append('Accept', 'application/json');
    return this.http.post(`${this.path}start`, null, { headers });
  }

  stop(): Observable<any> {
    const headers = new HttpHeaders()
      .append('Content-Type', 'application/json')
      .append('Accept', 'application/json');
    return this.http.post(`${this.path}stop`, null, { headers });
  }

  propose(value: string): Observable<any> {
    const headers = new HttpHeaders()
      .append('Content-Type', 'application/json')
      .append('Accept', 'application/json');
    return this.http.post(`${this.path}propose/${value}`, null, { headers });
  }

  state(): Observable<any> {
    const headers = new HttpHeaders()
      .append('Content-Type', 'application/json')
      .append('Accept', 'application/json');
    return this.http.get(this.path, { headers });
  }

  leaderFail(): Observable<any> {
    const headers = new HttpHeaders()
      .append('Content-Type', 'application/json')
      .append('Accept', 'application/json');
    return this.http.post(`${this.path}test/leader-fail`, null, { headers });
  }

  simultaneousFail(): Observable<any> {
    const headers = new HttpHeaders()
      .append('Content-Type', 'application/json')
      .append('Accept', 'application/json');
    return this.http.post(`${this.path}test/simultaneous-fail`, null, { headers });
  }

  slotSkip(): Observable<any> {
    const headers = new HttpHeaders()
      .append('Content-Type', 'application/json')
      .append('Accept', 'application/json');
    return this.http.post(`${this.path}test/slot-skip`, null, { headers });
  }
}
