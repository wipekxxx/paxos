import {Accepted, NodeId, ProposalId, Resolution} from '../message';

export class Learner<T> {
  nodeId: NodeId;
  quorumSize: number;

  proposals: Map<ProposalId, ProposalStatus<T>> = new Map();
  acceptors: Map<NodeId, ProposalId> = new Map();
  finalValue?: T = null;
  finalAcceptors: Set<NodeId> = new Set();
  finalProposalId?: ProposalId = null;

  constructor(nodeId: NodeId, quorumSize: number) {
    this.nodeId = nodeId;
    this.quorumSize = quorumSize;
  }

  receive(msg: Accepted<T>): Resolution<T> {
    if (this.finalValue) {
      if (msg.proposalId >= this.finalProposalId && msg.proposalValue === this.finalValue) {
        this.finalAcceptors.add(msg.nodeId);
      }
      return new Resolution(this.nodeId, this.finalValue);
    }

    const last = this.acceptors.get(msg.nodeId);

    if (last && msg.proposalId <= last) {
      return null;
    }

    this.acceptors.set(msg.nodeId, msg.proposalId);

    if (last) {
      const status = this.proposals.get(last);
      status.retainCount--;
      status.acceptors.delete(msg.nodeId);
      if (status.retainCount === 0) {
        this.proposals.delete(last);
      }
    }

    if (!this.proposals.has(msg.proposalId)) {
      this.proposals.set(msg.proposalId, new ProposalStatus(0, 0, new Set(), msg.proposalValue));
    }

    const pStatus = this.proposals.get(msg.proposalId);
    pStatus.acceptCount++;
    pStatus.retainCount++;
    pStatus.acceptors.add(msg.nodeId);

    if (pStatus.acceptCount === this.quorumSize) {
      this.finalProposalId = msg.proposalId;
      this.finalValue = msg.proposalValue;
      this.finalAcceptors = pStatus.acceptors;

      this.proposals = new Map();
      this.acceptors = new Map();
      return new Resolution(this.nodeId, msg.proposalValue);
    }

    return null;
  }
}

export class ProposalStatus<T> {
  acceptCount: number;
  retainCount: number;
  acceptors: Set<NodeId>;
  value: T;

  constructor(acceptCount: number, retainCount: number, acceptors: Set<NodeId>, value: T) {
    this.acceptCount = acceptCount;
    this.retainCount = retainCount;
    this.acceptors = acceptors;
    this.value = value;
  }
}
