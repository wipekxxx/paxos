import {Accept, Accepted, Message, Nack, NodeId, Prepare, Promis, ProposalId} from '../message';

export class Acceptor<T> {
  nodeId: NodeId;
  promisedId?: ProposalId = null;
  acceptedId?: ProposalId = null;
  acceptedValue?: T = null;

  constructor(nodeId: NodeId) {
    this.nodeId = nodeId;
  }

  receive(msg: Message): Message {
    console.log(msg);
    if (msg instanceof Prepare) {
      return this.receivePrepare(msg);
    } else if (msg instanceof Accept) {
      return this.receiveAccept(msg);
    }
    return null;
  }

  private receivePrepare(msg: Prepare) {
    console.log(msg);
    if (this.promisedId && this.promisedId <= msg.proposalId) {
      this.promisedId = msg.proposalId;
      return this.promise(msg);
    } else if (msg) {
      return this.promise(msg);
    }
    return new Nack(this.nodeId, msg.proposalId, msg.nodeId, this.promisedId);
  }

  private promise(msg: Prepare): Promis<T> {
    return new Promis(this.nodeId, msg.proposalId, msg.nodeId, this.acceptedId, this.acceptedValue);
  }

  private receiveAccept(msg: Accept<T>): Message {
    console.log(msg);
    if (this.promisedId && this.promisedId <= msg.proposalId) {
      return this.accept(msg);
    } else if (msg) {
      return this.accept(msg);
    }
    return new Nack(this.nodeId, msg.proposalId, msg.nodeId, this.promisedId);
  }

  private accept(msg: Accept<T>) {
    this.promisedId = msg.proposalId;
    this.acceptedId = msg.proposalId;
    this.acceptedValue = msg.proposalValue;
    return new Accepted(this.nodeId, msg.proposalId, msg.proposalValue);
  }
}
