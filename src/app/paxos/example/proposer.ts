import {Accept, Message, Nack, NodeId, Prepare, Promis, ProposalId} from '../message';

export class Proposer<T> {
  nodeId: NodeId;
  quorumSize: number;

  proposedValue?: T;
  proposalId: ProposalId = new ProposalId(0, this.nodeId);
  highestProposalId = new ProposalId(0, this.nodeId);
  highestAcceptedId?: ProposalId = null;
  promisesReceived: Set<NodeId> = new Set();
  nacksReceived: Set<NodeId> = new Set();
  currentPrepare?: Prepare = null;
  currentAccept?: Accept<T> = null;

  leader = false;
  history: Message[] = [];

  constructor(nodeId: NodeId, quorumSize: number) {
    this.nodeId = nodeId;
    this.quorumSize = quorumSize;
  }

  proposeValue(value: T): Accept<T> {
    if (!(this.proposedValue)) {
      this.proposedValue = value;
      if (this.leader) {
        this.currentAccept = new Accept(this.nodeId, this.proposalId, value);
        return this.currentAccept;
      }
    }
    return null;
  }

  prepare(): Prepare {
    this.leader = false;
    this.promisesReceived = new Set();
    this.nacksReceived = new Set();
    this.proposalId = new ProposalId(this.highestProposalId.proposalNumber + 1, this.nodeId);
    this.highestProposalId = this.proposalId;
    this.currentPrepare = new Prepare(this.nodeId, this.proposalId);
    return this.currentPrepare;
  }

  observeProposal(proposalId: ProposalId): void {
    if (proposalId.compare(this.highestProposalId)) {
      this.highestProposalId = proposalId;
    }
  }

  receive(msg: Message) {
    if (msg instanceof Nack) {
      return this.receiveNack(msg);
    } else if (msg instanceof Promis) {
      return this.receivePromise(msg);
    }
    return null;
  }

  private receiveNack(msg: Nack): Prepare {
    this.observeProposal(msg.proposalId);

    if (msg.proposalId.compare(this.proposalId) === 0) {
      this.nacksReceived = this.nacksReceived.add(msg.nodeId);
      if (this.nacksReceived.size === this.quorumSize) {
        return this.prepare();
      }
    }
    return null;
  }

  private receivePromise(msg: Promis<T>): Accept<T> {
    this.observeProposal(msg.proposalId);

    if (!this.leader && (msg.proposalId.compare(this.proposalId) === 0) && !this.promisesReceived.has(msg.nodeId)) {
      this.promisesReceived.add(msg.nodeId);

      if (msg.lastAcceptedProposalId != null && this.highestAcceptedId == null) {
        this.updateAccepted(msg.lastAcceptedProposalId, msg.lastAcceptedValue);
      } else if (msg.lastAcceptedProposalId != null && this.highestAcceptedId != null) {
        if (msg.lastAcceptedProposalId.compare(this.highestAcceptedId)) {
          this.updateAccepted(msg.lastAcceptedProposalId, msg.lastAcceptedValue);
        }
      }

      if (this.promisesReceived.size === this.quorumSize) {
        this.leader = true;
        if (this.proposedValue) {
          this.currentAccept = new Accept<T>(this.nodeId, this.proposalId, this.proposedValue);
          return this.currentAccept;
        }
      }
    }
    return null;
  }

  private updateAccepted(pid: ProposalId, value?: T) {
    this.highestAcceptedId = pid;
    if (value) {
      this.proposedValue = value;
    }
  }
}
