export class NodeId {
  value: string;

  constructor(value: string) {
    this.value = value;
  }
}

export class ProposalId {
  proposalNumber: number;
  nodeId: NodeId;


  constructor(proposalNumber: number, nodeId: NodeId) {
    this.proposalNumber = proposalNumber;
    this.nodeId = nodeId;
  }

  compare(that: ProposalId): number {
    return (this.proposalNumber < that.proposalNumber) ? -1
      : ((this.proposalNumber === that.proposalNumber) ?
        this.nodeId.value.localeCompare(that.nodeId.value) : 1);
  }
}

export abstract class Message {
}

export class Prepare extends Message {
  nodeId: NodeId;
  proposalId: ProposalId;

  constructor(nodeId: NodeId, proposalId: ProposalId) {
    super();
    this.nodeId = nodeId;
    this.proposalId = proposalId;
  }
}

export class Nack extends Message {
  nodeId: NodeId;
  proposalId: ProposalId;
  proposerNodeId: NodeId;
  promisedProposalId?: ProposalId;

  constructor(nodeId: NodeId, proposalId: ProposalId, proposerNodeId: NodeId, promisedProposalId: ProposalId) {
    super();
    this.nodeId = nodeId;
    this.proposalId = proposalId;
    this.proposerNodeId = proposerNodeId;
    this.promisedProposalId = promisedProposalId;
  }
}

export class Promis<T> {
  nodeId: NodeId;
  proposalId: ProposalId;
  proposerNodeId: NodeId;
  lastAcceptedProposalId?: ProposalId;
  lastAcceptedValue?: T;

  constructor(nodeId: NodeId, proposalId: ProposalId, proposerNodeId: NodeId, lastAcceptedProposalId: ProposalId, lastAcceptedValue: T) {
    this.nodeId = nodeId;
    this.proposalId = proposalId;
    this.proposerNodeId = proposerNodeId;
    this.lastAcceptedProposalId = lastAcceptedProposalId;
    this.lastAcceptedValue = lastAcceptedValue;
  }
}

export class Accept<T> extends Message {
  nodeId: NodeId;
  proposalId: ProposalId;
  proposalValue: T;

  constructor(nodeId: NodeId, proposalId: ProposalId, proposalValue: T) {
    super();
    this.nodeId = nodeId;
    this.proposalId = proposalId;
    this.proposalValue = proposalValue;
  }
}

export class Accepted<T> extends Message {
  nodeId: NodeId;
  proposalId: ProposalId;
  proposalValue: T;

  constructor(nodeId: NodeId, proposalId: ProposalId, proposalValue: T) {
    super();
    this.nodeId = nodeId;
    this.proposalId = proposalId;
    this.proposalValue = proposalValue;
  }
}

export class Resolution<T> extends Message {
  nodeId: NodeId;
  proposalValue: T;

  constructor(nodeId: NodeId, proposalValue: T) {
    super();
    this.nodeId = nodeId;
    this.proposalValue = proposalValue;
  }
}

export enum NodeStatus {
  UP = 'UP',
  DOWN = 'DOWN',
  UNREACHABLE = 'UNREACHABLE'
}

