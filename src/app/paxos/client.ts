import {Node} from './node';

export class Client {
  id: string;
  emitTime = 1000;
  nodes: Node<number>[] = [];
  value = 0;
  history: string[] = [];

  constructor(id: string, nodes: Node<number>[]) {
    this.id = id;
    this.nodes = nodes;
  }

  startEmit() {
    setTimeout(() => {
      this.emit();
      this.startEmit();
    }, this.emitTime);
  }

  emit() {
    const node = this.nodes[Math.floor(Math.random() * this.nodes.length)];
    this.history.push('Send: ' + ++this.value + ' to: ' + node.nodeId.value);
    node.proposeValue(this.value);
  }
}
