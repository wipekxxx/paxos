import {Accept, Accepted, Message, Nack, NodeId, NodeStatus, Prepare, Promis, ProposalId, Resolution} from './message';
import {ProposalStatus} from './example/learner';

export class Node<T> {
  nodeId: NodeId;
  quorumSize: number;
  promisedId?: ProposalId = null;
  acceptedId?: ProposalId = null;
  acceptedValue?: T = null;

  proposals: Map<ProposalId, ProposalStatus<T>> = new Map();
  acceptors: Map<NodeId, ProposalId> = new Map();
  finalValue?: T = null;
  finalAcceptors: Set<NodeId> = new Set();
  finalProposalId?: ProposalId = null;

  proposedValue?: T;
  proposalId: ProposalId = new ProposalId(0, this.nodeId);
  highestProposalId = new ProposalId(0, this.nodeId);
  highestAcceptedId?: ProposalId = null;
  promisesReceived: Set<NodeId> = new Set();
  nacksReceived: Set<NodeId> = new Set();
  currentPrepare?: Prepare = null;
  currentAccept?: Accept<T> = null;

  leader = false;

  status: NodeStatus = NodeStatus.UP;

  nodes: Node<T>[] = [];

  history: string[] = [];

  constructor(nodeId: NodeId, quorumSize: number) {
    this.nodeId = nodeId;
    this.quorumSize = quorumSize;
  }

  receive(msg: Message): Message {
    if (msg instanceof Prepare) {
      return this.receivePrepare(msg);
    } else if (msg instanceof Accept) {
      return this.receiveAccept(msg);
    } else if (msg instanceof Accepted) {
      return this.receiveAccepted(msg);
    } else if (msg instanceof Nack) {
      return this.receiveNack(msg);
    } else if (msg instanceof Promis) {
      return this.receivePromise(msg);
    }
    return null;
  }

  private receivePrepare(msg: Prepare) {
    this.history.push(this.nodeId.value + ' receivePrepare: ' + msg.nodeId.value + ' ' + msg.proposalId);
    if (this.promisedId && this.promisedId <= msg.proposalId) {
      this.promisedId = msg.proposalId;
      return this.promise(msg);
    } else if (msg) {
      return this.promise(msg);
    }
    return new Nack(this.nodeId, msg.proposalId, msg.nodeId, this.promisedId);
  }

  private promise(msg: Prepare): Promis<T> {
    return new Promis(this.nodeId, msg.proposalId, msg.nodeId, this.acceptedId, this.acceptedValue);
  }

  private receiveAccept(msg: Accept<T>): Message {
    this.history.push(this.nodeId.value + ' receiveAccept: ' + msg.nodeId.value + ' ' + msg.proposalId + ' ' + msg.proposalValue);
    if (this.promisedId && this.promisedId <= msg.proposalId) {
      return this.accept(msg);
    } else if (msg) {
      return this.accept(msg);
    }
    return new Nack(this.nodeId, msg.proposalId, msg.nodeId, this.promisedId);
  }

  private accept(msg: Accept<T>) {
    this.promisedId = msg.proposalId;
    this.acceptedId = msg.proposalId;
    this.acceptedValue = msg.proposalValue;
    return new Accepted(this.nodeId, msg.proposalId, msg.proposalValue);
  }

  receiveAccepted(msg: Accepted<T>): Resolution<T> {
    this.history.push(this.nodeId.value + ' receiveAccepted: ' + msg.nodeId.value + ' ' + msg.proposalId + ' ' + msg.proposalValue);
    if (this.finalValue) {
      if (msg.proposalId >= this.finalProposalId && msg.proposalValue === this.finalValue) {
        this.finalAcceptors.add(msg.nodeId);
      }
      return new Resolution(this.nodeId, this.finalValue);
    }

    const last = this.acceptors.get(msg.nodeId);

    if (last && msg.proposalId <= last) {
      return null;
    }

    this.acceptors.set(msg.nodeId, msg.proposalId);

    if (last) {
      const status = this.proposals.get(last);
      status.retainCount--;
      status.acceptors.delete(msg.nodeId);
      if (status.retainCount === 0) {
        this.proposals.delete(last);
      }
    }

    if (!this.proposals.has(msg.proposalId)) {
      this.proposals.set(msg.proposalId, new ProposalStatus(0, 0, new Set(), msg.proposalValue));
    }

    const pStatus = this.proposals.get(msg.proposalId);
    pStatus.acceptCount++;
    pStatus.retainCount++;
    pStatus.acceptors.add(msg.nodeId);

    if (pStatus.acceptCount === this.quorumSize) {
      this.finalProposalId = msg.proposalId;
      this.finalValue = msg.proposalValue;
      this.finalAcceptors = pStatus.acceptors;

      this.proposals = new Map();
      this.acceptors = new Map();
      return new Resolution(this.nodeId, msg.proposalValue);
    }

    return null;
  }

  proposeValue(value: T): Accept<T> {
    this.history.push(this.nodeId.value + ' proposeValue ' + value);
    if (!(this.proposedValue)) {
      this.proposedValue = value;
      if (this.leader) {
        this.currentAccept = new Accept(this.nodeId, this.proposalId, value);
        return this.currentAccept;
      }
    }
    const promises = this.nodes.map(node => node.receive(this.prepare()));
    console.log(promises);
    this.currentAccept = new Accept(this.nodeId, this.proposalId, value);
    const accepted = this.nodes.map(node => node.receive(this.accept(this.currentAccept)));
    console.log(accepted);
    const resolutions = this.nodes.map(node => node.receive(accepted[0]));
    console.log(resolutions);
    return this.currentAccept;
  }

  prepare(): Prepare {
    this.leader = false;
    this.promisesReceived = new Set();
    this.nacksReceived = new Set();
    this.proposalId = new ProposalId(this.highestProposalId.proposalNumber + 1, this.nodeId);
    this.highestProposalId = this.proposalId;
    this.currentPrepare = new Prepare(this.nodeId, this.proposalId);
    return this.currentPrepare;
  }

  observeProposal(proposalId: ProposalId): void {
    if (proposalId.compare(this.highestProposalId)) {
      this.highestProposalId = proposalId;
    }
  }

  private receiveNack(msg: Nack): Prepare {
    this.history.push(this.nodeId.value + ' receiveNack: ' + msg.nodeId.value + ' ' +
      msg.proposalId + ' ' + msg.proposerNodeId + ' ' + msg.promisedProposalId);
    this.observeProposal(msg.proposalId);

    if (msg.proposalId.compare(this.proposalId) === 0) {
      this.nacksReceived = this.nacksReceived.add(msg.nodeId);
      if (this.nacksReceived.size === this.quorumSize) {
        return this.prepare();
      }
    }
    return null;
  }

  private receivePromise(msg: Promis<T>): Accept<T> {
    this.history.push(this.nodeId.value + ' receivePromise: ' + msg.nodeId.value + ' ' + msg.proposalId + ' ' + msg.proposerNodeId
      + ' ' + msg.lastAcceptedProposalId + ' ' + msg.lastAcceptedValue);
    this.observeProposal(msg.proposalId);

    if (!this.leader && (msg.proposalId.compare(this.proposalId) === 0) && !this.promisesReceived.has(msg.nodeId)) {
      this.promisesReceived.add(msg.nodeId);

      if (msg.lastAcceptedProposalId != null && this.highestAcceptedId == null) {
        this.updateAccepted(msg.lastAcceptedProposalId, msg.lastAcceptedValue);
      } else if (msg.lastAcceptedProposalId != null && this.highestAcceptedId != null) {
        if (msg.lastAcceptedProposalId.compare(this.highestAcceptedId)) {
          this.updateAccepted(msg.lastAcceptedProposalId, msg.lastAcceptedValue);
        }
      }

      if (this.promisesReceived.size === this.quorumSize) {
        this.leader = true;
        if (this.proposedValue) {
          this.currentAccept = new Accept<T>(this.nodeId, this.proposalId, this.proposedValue);
          return this.currentAccept;
        }
      }
    }
    return null;
  }

  private updateAccepted(pid: ProposalId, value?: T) {
    this.highestAcceptedId = pid;
    if (value) {
      this.proposedValue = value;
    }
  }

  up() {
    this.status = NodeStatus.UP;
  }

  down() {
    this.status = NodeStatus.DOWN;
  }

  unreachable() {
    this.status = NodeStatus.UNREACHABLE;
  }

  sleep(ms) {
    // await sleep(2000);
    return new Promise(resolve => setTimeout(resolve, ms));
  }
}
